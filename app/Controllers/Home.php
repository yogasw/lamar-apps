<?php namespace App\Controllers;

use CodeIgniter\Controller;

class Home extends BaseController
{
    public $data;

	public function index()
	{
	    $this->data = array(
	      "data" => view('home')
        );

		return view('template',$this->data);
	}

    public function uji()
    {
        return view('welcome_message');
    }

	//--------------------------------------------------------------------

}
